import { CommunicationProtocolEnum, DaprClient } from "dapr-client";

const staffDirectoryService = `http://localhost:${process.env.DAPR_HTTP_PORT}/v1.0/invoke/staff-directory-service/method/api/directories`;

const daprHost = "127.0.0.1";
const STATE_STORE_NAME = "statestore";

const client = new DaprClient(
  daprHost,
  process.env.DAPR_HTTP_PORT,
  CommunicationProtocolEnum.HTTP
);

export default async function handler(req, res) {
  const visitor = req.body;
  const staff = await findStaff(visitor.visitingStaff);

  visitor.staff = staff;
  var status = await checkInStaff(visitor);

  res.status(200).json(status);
}

async function checkInStaff(visitor) {
  return new Promise(async (resolve, reject) => {
    try {
      await client.state.save(STATE_STORE_NAME, [
        {
          key: visitor.email,
          value: visitor,
        },
      ]);

      resolve({ status: "DONE" });
    } catch (e) {
      console.warn(e);
      reject("BAD BAD BAD.....");
    }
  });
}

async function findStaff(email) {
  return new Promise(async (resolve, reject) => {
    const response = await fetch(
      staffDirectoryService + "?filters[email][$eq]=" + email
    );

    if (response.status === 200) {
      const obj = await response.json();
      const { Name, Email } = obj.data[0].attributes;
      resolve({
        name: Name,
        email: Email,
      });
    } else {
      reject("BAD BAD BAD.....");
    }
  });
}
