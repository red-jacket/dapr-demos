module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '5cef1a741f8d9813723f2e3fec55ba89'),
  },
});
