// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
};
const daprPort = process.env.DAPR_HTTP_PORT || 3500;

const staffDirectoryService = `http://localhost:${daprPort}/v1.0/invoke/visitor-checkin-service/method/api/checkin`;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const response = await fetch(staffDirectoryService, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(req.body),
  });

  if (response.status === 200) {
    res.status(200).json({
      name: "done",
    });
  } else {
    console.warn(response.statusText);
    res.status(400).json({
      name: "something went wwwwrong",
    });
  }
}
