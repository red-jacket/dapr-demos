# dapr-example

dapr run --app-port 5001 --dapr-http-port 3501 --app-id staff-directory-service npm run develop

dapr run --app-port 6001 --dapr-http-port 3601 --app-id visitor-checkin-service npm run dev

dapr run --app-port 3000 --dapr-http-port 3301 --app-id visitor-app npm run dev
